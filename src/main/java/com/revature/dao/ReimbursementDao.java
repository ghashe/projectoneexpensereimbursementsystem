package com.revature.dao;

import com.revature.models.Reimbursement;

import java.util.List;

public interface ReimbursementDao {

    public List<Reimbursement> getAllReimbursements();
    public List<Reimbursement> getReimbursementsByAuthor(int authorId);
    public Reimbursement getReimbursementById(int reimbId);

    public boolean requestReimbursement(int authorId, int statusId, int typeId, Reimbursement reimbursement);
    public boolean requestReimbursement(int amount, String description, int authorId, int typeId);
    public boolean approveReimbursement(int reimbId);
    public boolean denyReimbursement(int reimbId);
    public boolean deleteReimbursementByAuthor(int authorId);

}
