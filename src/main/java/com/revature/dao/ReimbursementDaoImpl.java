package com.revature.dao;

import com.revature.models.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//Singleton
public class ReimbursementDaoImpl implements ReimbursementDao{
    Connection conn = null;	// Our connection to the database
    PreparedStatement stmt = null;	// We use prepared statements to help protect against SQL injection

    private static ReimbursementDao reimbursementDao;

    ReimbursementDaoImpl(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public  static ReimbursementDao getInstance(){
        if(reimbursementDao == null)
            reimbursementDao = new ReimbursementDaoImpl();
        return reimbursementDao;
    }

    @Override
    public List<Reimbursement> getAllReimbursements() {

        List<Reimbursement> reimbursements = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_reimbursement"; // Our SQL query

            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();  // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()) {
                // We need to populate a Reimbursement object with info for each row from our query result
                Reimbursement reimbursement = new Reimbursement();

                // Each variable in our Reimbursement object maps to a column in a row from our results.
                reimbursement.setReimbId(rs.getInt("reimb_id"));
                reimbursement.setAmount(rs.getInt("reimb_amount"));
                reimbursement.setDateSubmitted(rs.getDate("reimb_submitted"));
                reimbursement.setDateResolved(rs.getDate("reimb_resolved"));
                reimbursement.setDescription(rs.getString("reimb_description"));
                reimbursement.setReceipt(rs.getBytes("reimb_receipt"));
                reimbursement.setAuthor(rs.getInt("reimb_author"));
                reimbursement.setResolver(rs.getInt("reimb_resolver"));
                reimbursement.setStatusId(rs.getInt("reimb_status_id"));
                reimbursement.setTypeId(rs.getInt("reimb_type_id"));

                // Finally we add it to the list of Reimbursement objects returned by this query.
                reimbursements.add(reimbursement);
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // We need to make sure our statements and connections are closed,
            // or else we could wind up with a memory leak
            closeResources();
        }

        // return the list of Reimbursement objects populated by the DB.
        return reimbursements;
}

    @Override
    public List<Reimbursement> getReimbursementsByAuthor(int authorId) {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_reimbursement where reimb_author = ?"; // Our SQL query
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, authorId);

            ResultSet rs = stmt.executeQuery();  // Queries the database

            while (rs.next()) {
                // We need to populate a Reimbursement object with info for each row from our query result
                Reimbursement reimbursement = new Reimbursement();

                // Each variable in our Reimbursement object maps to a column in a row from our results.
                reimbursement.setReimbId(rs.getInt("reimb_id"));
                reimbursement.setAmount(rs.getInt("reimb_amount"));
                reimbursement.setDateSubmitted(rs.getDate("reimb_submitted"));
                reimbursement.setDateResolved(rs.getDate("reimb_resolved"));
                reimbursement.setDescription(rs.getString("reimb_description"));
                reimbursement.setReceipt(rs.getBytes("reimb_receipt"));
                reimbursement.setAuthor(rs.getInt("reimb_author"));
                reimbursement.setResolver(rs.getInt("reimb_resolver"));
                reimbursement.setStatusId(rs.getInt("reimb_status_id"));
                reimbursement.setTypeId(rs.getInt("reimb_type_id"));

                reimbursements.add(reimbursement);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }

        return reimbursements;
    }

    @Override
    public Reimbursement getReimbursementById(int reimbId) {

        Reimbursement reimbursement = null;

        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_reimbursement where reimb_id = ?"; // Our SQL query
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, reimbId);

            ResultSet rs = stmt.executeQuery();  // Queries the database

            if (rs.next()) {
                reimbursement = new Reimbursement();
                // We need to populate a Reimbursement object with info for each row from our query result

                // Each variable in our Reimbursement object maps to a column in a row from our results.
                reimbursement.setReimbId(rs.getInt("reimb_id"));
                reimbursement.setAmount(rs.getInt("reimb_amount"));
                reimbursement.setDateSubmitted(rs.getDate("reimb_submitted"));
                reimbursement.setDateResolved(rs.getDate("reimb_resolved"));
                reimbursement.setDescription(rs.getString("reimb_description"));
                reimbursement.setReceipt(rs.getBytes("reimb_receipt"));
                reimbursement.setAuthor(rs.getInt("reimb_author"));
                reimbursement.setResolver(rs.getInt("reimb_resolver"));
                reimbursement.setStatusId(rs.getInt("reimb_status_id"));
                reimbursement.setTypeId(rs.getInt("reimb_type_id"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }

        return reimbursement;
    }

    @Override
    public boolean requestReimbursement(int authorId, int statusId, int typeId, Reimbursement reimbursement) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_reimbursement (reimb_amount, reimb_description, reimb_receipt, reimb_author, reimb_type_id) VALUES(?, ?, ?, ?, ?)"; // Our SQL query
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

        stmt.setInt(1, reimbursement.getAmount());
        stmt.setString(2, reimbursement.getDescription());
        stmt.setBytes(3, reimbursement.getReceipt());
        stmt.setInt(4, reimbursement.getAuthor());
        stmt.setInt(5, reimbursement.getTypeId());

        // If we were able to add our reimbursement to the database, we want to return true.
        // This if statement both executes our query, and looks at the return
        // value to determine how many rows were changed
        if (stmt.executeUpdate() != 0)
            return true;
        else
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean requestReimbursement(int amount, String description, int authorId, int typeId) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_reimbursement (reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_receipt, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id)\n" +
                    "VALUES(?, CURRENT_TIMESTAMP, default, ?, default, ?, default, default, ?)"; // Our SQL query
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, amount);
            stmt.setString(2, description);
            stmt.setInt(3, authorId);
            stmt.setInt(4, typeId);

            // If we were able to add our reimbursement to the database, we want to return true.
            // This if statement both executes our query, and looks at the return
            // value to determine how many rows were changed
            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean approveReimbursement(int reimbId){
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "UPDATE public.ers_reimbursement\n" +
                    "SET reimb_status_id=2\n" +
                    "WHERE reimb_id=?;";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, reimbId);



        System.out.println(stmt);

        if (stmt.executeUpdate() != 0)
            return true;
        else
            return false;

    } catch (SQLException e) {
        e.printStackTrace();
        return false;
    } finally {
        closeResources();
    }

}
    @Override
    public boolean denyReimbursement(int reimbId) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "UPDATE public.ers_reimbursement\n" +
                    "SET reimb_status_id=3\n" +
                    "WHERE reimb_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, reimbId);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean deleteReimbursementByAuthor(int authorId) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "DELETE FROM public.ers_reimbursement\n" +
                    "WHERE reimb_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, authorId);

        if (stmt.executeUpdate() != 0)
            return true;
        else
            return false;

        } catch (SQLException e) {
        e.printStackTrace();
        return false;
        } finally {
        closeResources();
        }
    }

// Closing all resources is important, to prevent memory leaks.
// Ideally, you really want to close them in the reverse-order you open them

    private void closeResources() {
        try {
        if (stmt != null)
            stmt.close();
        } catch (SQLException e) {
        System.out.println("Could not close statement!");
        e.printStackTrace();
        }

        try {
        if (conn != null)
        conn.close();
        } catch (SQLException e) {
        System.out.println("Could not close connection!");
        e.printStackTrace();
        }
    }

}
