package com.revature.dao;

import com.revature.models.Reimbursement;
import com.revature.models.ReimbursementStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementStatusDaoImpl implements ReimbursementStatusDao {
    Connection conn = null;    // Our connection to the database
    PreparedStatement stmt = null;    // We use prepared statements to help protect against SQL injection

    private static ReimbursementStatusDao reimbursementStatusDao;

    ReimbursementStatusDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ReimbursementStatusDao getInstance() {
        if (reimbursementStatusDao == null)
            reimbursementStatusDao = new ReimbursementStatusDaoImpl();
        return reimbursementStatusDao;
    }

    @Override
    public List<ReimbursementStatus> getAllReimbursementStatus() {

        List<ReimbursementStatus> reimbursementStatuses = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_reimbursement_status"; // Our SQL query

            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();  // Queries the database

            // So long as the ResultSet actually contains results...
            while (rs.next()) {
                // We need to populate a ReimbursementStatus object with info for each row from our query result
                ReimbursementStatus reimbursementStatus = new ReimbursementStatus();

                // Each variable in our ReimbursementStatus object maps to a column in a row from our results.
                reimbursementStatus.setId(rs.getInt("reimb_ststus_id"));
                reimbursementStatus.setStatus(rs.getString("reimb_status"));

                // Finally we add it to the list of Reimbursement objects returned by this query.
                reimbursementStatuses.add(reimbursementStatus);
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // We need to make sure our statements and connections are closed,
            // or else we could wind up with a memory leak
            closeResources();
        }

        // return the list of Reimbursement objects populated by the DB.
        return reimbursementStatuses;
    }

    @Override
    public boolean addReimbursementStatus(String status) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_reimbursement_status\n" +
                    "(reimb_status)\n" +
                    "VALUES(?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, status);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean updateReimbursementStatus(String status, int id) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "UPDATE public.ers_reimbursement_status\n" +
                    "SET reimb_status=?\n" +
                    "WHERE reimb_ststus_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1,status);
            stmt.setInt(2, id);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean deleteReimbursementStatusById(int id) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "DELETE FROM public.ers_reimbursement_status\n" +
                    "WHERE reimb_ststus_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

           stmt.setInt(1, id);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
