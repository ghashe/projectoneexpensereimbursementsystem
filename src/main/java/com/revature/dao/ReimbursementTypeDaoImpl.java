package com.revature.dao;

import com.revature.models.ReimbursementStatus;
import com.revature.models.ReimbursementType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementTypeDaoImpl implements ReimbursementTypeDao{
    Connection conn = null;    // Our connection to the database
    PreparedStatement stmt = null;    // We use prepared statements to help protect against SQL injection

    private static ReimbursementTypeDao reimbursementTypeDao;

    ReimbursementTypeDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ReimbursementTypeDao getInstance() {
        if (reimbursementTypeDao == null)
            reimbursementTypeDao = new ReimbursementTypeDaoImpl();
        return reimbursementTypeDao;
    }

    @Override
    public List<ReimbursementType> getAllReimbursementTypes() {

        List<ReimbursementType> reimbursementTypes = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_reimbursement_type"; // Our SQL query

            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();  // Queries the database

            while (rs.next()) {
                ReimbursementType reimbursementType = new ReimbursementType();

                reimbursementType.setId(rs.getInt("reimb_type_id"));
                reimbursementType.setType(rs.getString("reimb_type"));

                reimbursementTypes.add(reimbursementType);
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // We need to make sure our statements and connections are closed,
            // or else we could wind up with a memory leak
            closeResources();
        }

        // return the list of Reimbursement objects populated by the DB.
        return reimbursementTypes;
    }

    @Override
    public boolean addReimbursementType(String type) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_reimbursement_type\n" +
                    "(reimb_type)\n" +
                    "VALUES(?);";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1, type);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean updateReimbursementType( int id, String type) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "UPDATE public.ers_reimbursement_type\n" +
                    "SET reimb_type=?\n" +
                    "WHERE reimb_type_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1,type);
            stmt.setInt(2, id);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean deleteReimbursementTypeById(int id) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "DELETE FROM public.ers_reimbursement_type\n" +
                    "WHERE reimb_type_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, id);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }


    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
