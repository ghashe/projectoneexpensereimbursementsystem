package com.revature.dao;

import com.revature.models.User;

public interface UserDao {
    public boolean insertUser(User user);
    public User getOneUser(String username);
}
