package com.revature.dao;

import com.revature.models.Reimbursement;
import com.revature.models.ReimbursementType;
import com.revature.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    Connection conn = null;    // Our connection to the database
    PreparedStatement stmt = null;    // We use prepared statements to help protect against SQL injection

    private static UserDao userDao;

    UserDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static UserDao getInstance() {
        if (userDao == null)
            userDao = new UserDaoImpl();
        return userDao;
    }

    @Override
    public boolean insertUser(User user) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_users\n" +
                    "(ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id)\n" +
                    "VALUES(?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getFirstName());
            stmt.setString(4, user.getLastName());
            stmt.setString(5, user.getEmail());
            stmt.setInt(6, user.getRoleId());

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public User getOneUser(String username) {

        User user = null;

        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_users\n" +
                    "where ers_username = ?"; // Our SQL query
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1, username);

            ResultSet rs = stmt.executeQuery();  // Queries the database

            if (rs.next()) {
                user = new User();
                // We need to populate a Reimbursement object with info for each row from our query result

                // Each variable in our Reimbursement object maps to a column in a row from our results.
                user.setId(rs.getInt("ers_users_id"));
                user.setUsername(rs.getString("ers_username"));
                user.setPassword(rs.getString("ers_password"));
                user.setFirstName(rs.getString("user_first_name"));
                user.setLastName(rs.getString("user_last_name"));
                user.setEmail(rs.getString("user_email"));
                user.setRoleId(rs.getInt("user_role_id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return user;
    }


    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
