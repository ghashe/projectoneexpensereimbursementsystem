package com.revature.dao;

import com.revature.models.UserRole;

import java.util.List;

public interface UserRoleDao {
    public List<UserRole> getAllUserRoles();

    public boolean addUserRole(String userRole);
    public boolean updateUserRole(int id, String Role);
    public boolean deleteUserRoleById(int id);
}
