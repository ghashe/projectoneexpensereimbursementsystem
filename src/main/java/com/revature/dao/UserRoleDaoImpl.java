package com.revature.dao;

import com.revature.models.ReimbursementType;
import com.revature.models.UserRole;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRoleDaoImpl implements UserRoleDao{
    Connection conn = null;    // Our connection to the database
    PreparedStatement stmt = null;    // We use prepared statements to help protect against SQL injection

    private static UserRoleDao userRoleDao;

    UserRoleDaoImpl() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static UserRoleDao getInstance() {
        if (userRoleDao == null)
            userRoleDao = new UserRoleDaoImpl();
        return userRoleDao;
    }

    @Override
    public List<UserRole> getAllUserRoles() {
        List<UserRole> userRoles = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "SELECT * FROM public.ers_user_roles"; // Our SQL query

            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            ResultSet rs = stmt.executeQuery();  // Queries the database

            while (rs.next()) {
                UserRole userRole = new UserRole();

                userRole.setId(rs.getInt("ers_user_role_id"));
                userRole.setRole(rs.getString("user_role"));

                userRoles.add(userRole);
            }

            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // We need to make sure our statements and connections are closed,
            // or else we could wind up with a memory leak
            closeResources();
        }

        // return the list of Reimbursement objects populated by the DB.
        return userRoles;
    }

    @Override
    public boolean addUserRole(String userRole) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "INSERT INTO public.ers_user_roles\n" +
                    "(user_role)\n" +
                    "VALUES(?)";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1, userRole);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean updateUserRole(int id, String Role) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "UPDATE public.ers_user_roles\n" +
                    "SET user_role=?\n" +
                    "WHERE ers_user_role_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setString(1,Role);
            stmt.setInt(2, id);

            System.out.println(stmt);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    @Override
    public boolean deleteUserRoleById(int id) {
        try {
            conn = DriverManager.getConnection(ConnectionUtil.url, ConnectionUtil.username, ConnectionUtil.password);
            String sql = "DELETE FROM public.ers_user_roles\n" +
                    "WHERE ers_user_role_id=?";
            stmt = conn.prepareStatement(sql);	// Creates the prepared statement from the query

            stmt.setInt(1, id);

            if (stmt.executeUpdate() != 0)
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeResources();
        }
    }

    // Closing all resources is important, to prevent memory leaks.
    // Ideally, you really want to close them in the reverse-order you open them
    private void closeResources() {
        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException e) {
            System.out.println("Could not close statement!");
            e.printStackTrace();
        }

        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection!");
            e.printStackTrace();
        }
    }
}
