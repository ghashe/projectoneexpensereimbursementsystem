package com.revature.frontcontroller;

import com.revature.controller.UserController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//endpoint routing
@WebServlet(name="dispatcher", urlPatterns = "/*")
public class Dispatcher extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String URI = req.getRequestURI();
        System.out.println(URI);

       switch (URI){
           case "/api/login":
               if(req.getMethod().equals("POST"))
               UserController.getInstance().login(req, resp);
               break;
       }
    }
}
