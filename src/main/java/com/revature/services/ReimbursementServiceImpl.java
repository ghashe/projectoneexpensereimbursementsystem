package com.revature.services;

import com.revature.dao.ReimbursementDao;
import com.revature.dao.ReimbursementDaoImpl;
import com.revature.models.Reimbursement;

import java.util.List;

public class ReimbursementServiceImpl implements ReimbursementService{
    ReimbursementDao reimbursementDao;


    public ReimbursementServiceImpl(){
        reimbursementDao = ReimbursementDaoImpl.getInstance();
    }

    @Override
    public List<Reimbursement> getAllReimbursements() {
        return reimbursementDao.getAllReimbursements();
    }

    @Override
    public List<Reimbursement> getReimbursementsByAuthor(int authorId) {
        return reimbursementDao.getReimbursementsByAuthor(authorId);
    }

    @Override
    public Reimbursement getReimbursementById(int reimbId) {
        return reimbursementDao.getReimbursementById(reimbId);
    }

    @Override
    public boolean requestReimbursement(int authorId, int statusId, int typeId, Reimbursement reimbursement) {
        return reimbursementDao.requestReimbursement(authorId, statusId, typeId, reimbursement);
    }

    @Override
    public boolean requestReimbursement(int amount, String description, int authorId, int typeId) {
        return reimbursementDao.requestReimbursement(amount, description, authorId,typeId);
    }

    @Override
    public boolean approveReimbursement(int reimbId) {
        return reimbursementDao.approveReimbursement(reimbId);
    }

    @Override
    public boolean denyReimbursement(int reimbId) {
        return reimbursementDao.denyReimbursement(reimbId);
    }

    @Override
    public boolean deleteReimbursementByAuthor(int authorId) {
        return reimbursementDao.deleteReimbursementByAuthor(authorId);
    }
}
