package com.revature.services;

import com.revature.models.ReimbursementStatus;

import java.util.List;

public interface ReimbursementStatusService {
    public List<ReimbursementStatus> getAllReimbursementStatus();

    public boolean addReimbursementStatus(String status );
    public boolean updateReimbursementStatus(String status, int id);
    public boolean deleteReimbursementStatusById(int id);
}
