package com.revature.services;

import com.revature.dao.ReimbursementStatusDao;
import com.revature.dao.ReimbursementStatusDaoImpl;
import com.revature.models.ReimbursementStatus;

import java.util.List;

public class ReimbursementStatusServiceImpl implements ReimbursementStatusService{
    ReimbursementStatusDao reimbursementStatusDao;

    ReimbursementStatusServiceImpl(){
        reimbursementStatusDao = ReimbursementStatusDaoImpl.getInstance();
    }
    @Override
    public List<ReimbursementStatus> getAllReimbursementStatus() {
        return reimbursementStatusDao.getAllReimbursementStatus();
    }

    @Override
    public boolean addReimbursementStatus(String status) {
        return reimbursementStatusDao.addReimbursementStatus(status);
    }

    @Override
    public boolean updateReimbursementStatus(String status, int id) {
        return reimbursementStatusDao.updateReimbursementStatus(status,id);
    }

    @Override
    public boolean deleteReimbursementStatusById(int id) {
        return reimbursementStatusDao.deleteReimbursementStatusById(id);
    }
}
