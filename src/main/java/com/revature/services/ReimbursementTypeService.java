package com.revature.services;

import com.revature.models.ReimbursementType;

import java.util.List;

public interface ReimbursementTypeService {
    public List<ReimbursementType> getAllReimbursementTypes();

    public boolean addReimbursementType(String type);
    public boolean updateReimbursementType(int id, String type);
    public boolean deleteReimbursementTypeById(int id);
}
