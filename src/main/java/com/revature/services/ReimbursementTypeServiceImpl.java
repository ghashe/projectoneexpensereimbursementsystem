package com.revature.services;

import com.revature.dao.ReimbursementTypeDao;
import com.revature.dao.ReimbursementTypeDaoImpl;
import com.revature.models.ReimbursementType;

import java.util.List;

public class ReimbursementTypeServiceImpl implements ReimbursementTypeService{

    ReimbursementTypeDao reimbursementTypeDao;

    ReimbursementTypeServiceImpl (){
        reimbursementTypeDao = ReimbursementTypeDaoImpl.getInstance();
    }
    @Override
    public List<ReimbursementType> getAllReimbursementTypes() {
        return reimbursementTypeDao.getAllReimbursementTypes();
    }

    @Override
    public boolean addReimbursementType(String type) {
        return reimbursementTypeDao.addReimbursementType(type);
    }

    @Override
    public boolean updateReimbursementType(int id, String type) {
        return reimbursementTypeDao.updateReimbursementType(id,type);
    }

    @Override
    public boolean deleteReimbursementTypeById(int id) {
        return reimbursementTypeDao.deleteReimbursementTypeById(id);
    }
}
