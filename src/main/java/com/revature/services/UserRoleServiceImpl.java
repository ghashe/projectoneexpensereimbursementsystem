package com.revature.services;

import com.revature.dao.UserRoleDao;
import com.revature.dao.UserRoleDaoImpl;
import com.revature.models.UserRole;

import java.util.List;

public class UserRoleServiceImpl implements UserRoleService {

    UserRoleDao userRoleDao;

    UserRoleServiceImpl (){
        userRoleDao = UserRoleDaoImpl.getInstance();
    }

    @Override
    public List<UserRole> getAllUserRoles() {
        return userRoleDao.getAllUserRoles();
    }

    @Override
    public boolean addUserRole(String userRole) {
        return userRoleDao.addUserRole(userRole);
    }

    @Override
    public boolean updateUserRole(int id, String Role) {
        return userRoleDao.updateUserRole(id, Role);
    }

    @Override
    public boolean deleteUserRoleById(int id) {
        return userRoleDao.deleteUserRoleById(id);
    }
}
