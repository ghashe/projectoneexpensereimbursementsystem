package com.revature.services;

import com.revature.models.User;

public interface UserService {
    public boolean register(User user);
    public User login(User user);
}
